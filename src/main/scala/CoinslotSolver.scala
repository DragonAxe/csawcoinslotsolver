import java.io.PrintStream
import java.net.Socket
import java.util.Scanner

import scala.util.Try

/**
  * Coinslot was one of the challenges in the 2016 CSAW CTF competition.
  * This program was designed to interact with CSAW's coinslot server.
  * First, the server would supply a dollar amount to the user, then
  * it would ask how many $10000, $5000, $1000, ..., $0.10, $0.05, $0.01
  * bills it takes to add up the the given dollar amount.
  *
  * A typical session with the CSAW server would look like this:
  * $0.06
  * $10,000 bills: 0
  * $5,000 bills: 0
  * $1,000 bills: 0
  * $500 bills: 0
  * $100 bills: 0
  * $50 bills: 0
  * $20 bills: 0
  * $10 bills: 0
  * $5 bills: 0
  * $1 bills: 0
  * half-dollars (50c): 0
  * quarters (25c): 0
  * dimes (10c): 0
  * nickels (5c): 1
  * pennies (1c): 1
  * correct!
  * $0.52
  * $10,000 bills: 0
  * $5,00 bills: 0
  * ...
  * And so-on until the server returns the CTF flag.
  */
object CoinslotSolver extends App {

  // Test helper functions
  //  println(getPartitions(getCents("$71247.88")))

  mainLogic()

  /** Connects to the CSAW coinslot server and runs until the CTF flag is returned. */
  def mainLogic() = {
    // Open a connection to the CSAW coinslot server
    val socket = new Socket("misc.chal.csaw.io", 8000)

    val inStream = socket.getInputStream
    val outStream = new PrintStream(socket.getOutputStream)
    val scanner = new Scanner(inStream)

    /** A helper function for reading words from the coinslot server. */
    def scannerNext() = {
      val line = scanner.next()
      print(line)
      line
    }

    /** A helper function for reading lines from the coinslot server. */
    def scannerNextline() = {
      val line = scanner.nextLine()
      println(line)
      line
    }

    /** The main loop.
      * Each iteration represents getting input from the coinslot server,
      * splitting the dollar amount into its various denominations, and
      * sending the denominations back to the coinslot server.
      */
    while (scanner.hasNext) {
      println()
      print("                   ")
      val amount = scannerNext()
      println()
      val p = getPartitions(getCents(amount))
      p.foreach(x => {
        outStream.println(x)
        Try {
          scannerNext()
          print(" ")
          scannerNext()
          print(" ")
          print(x)
          println()
        }
      })
      scannerNextline()
    }
  }

  /** Takes in a dollar amount in the following format: "$xxxxx.xx",
    * and outputs the number of cents that make up the dollar amount.
    * Example: "$2350.87" => 235087.
    *
    * @param amount The dollar amount in the format: "$xxxxx.xx".
    * @return The number of cents that make up the given dollar amount.
    */
  def getCents(amount: String): Int = {
    val index = amount.indexOf('.')
    (amount.substring(1, index) + amount.substring(index + 1)).toInt
  }

  /** Divides the total amount given into each denominator.
    * Example: 7124788 => List(7, 0, 1, 0, 2, 0, 2, 0, 1, 2, 1, 1, 1, 0, 3)
    *
    * @param totalAmount The total amount in cents.
    * @return A list of the number of each type of bill it takes to make the total amount that was given.
    */
  def getPartitions(totalAmount: Int) = {
    val denoms = List(1000000, 500000, 100000, 50000, 10000, 5000, 2000, 1000, 500, 100, 50, 25, 10, 5, 1)
    // Generate list of tuples with intermediate amounts and denominator multipliers
    denoms.scanLeft((totalAmount, 0))((centsmultpair, denom) => (centsmultpair._1 % denom, centsmultpair._1 / denom))
      // Remove first dummy element that scanLeft makes
      .tail
      // Keep only the multiplier part of the tuple
      .map((centsmultpair) => centsmultpair._2)
  }

}
