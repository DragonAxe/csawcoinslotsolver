# CSAW CTF Coinslot Solver

Coinslot was one of the challenges in the 2016 CSAW CTF competition.
This program was designed to interact with CSAW's coinslot server until the CTF flag is returned.

First, the server would supply a dollar amount to the user, then
it would ask how many $10000, $5000, $1000, ..., $0.10, $0.05, $0.01
bills it takes to add up the the given dollar amount.

This program loops doing the following until the flag is returned:

*  Get input from the coinslot server,
*  Split the dollar amount into its various denominations, and
*  Sending the denominations back to the coinslot server.


A typical session with the CSAW server would look like this:
```
$0.06
$10,000 bills: 0
$5,000 bills: 0
$1,000 bills: 0
$500 bills: 0
$100 bills: 0
$50 bills: 0
$20 bills: 0
$10 bills: 0
$5 bills: 0
$1 bills: 0
half-dollars (50c): 0
quarters (25c): 0
dimes (10c): 0
nickels (5c): 1
pennies (1c): 1
correct!
$0.52
$10,000 bills: 0
$5,000 bills: 0
$1,000 bills: 0
$500 bills: 0
$100 bills: 0
...
```
And so-on until the server returns the CTF flag.

The flag I found was this:
flag{started-from-the-bottom-now-my-whole-team-fucking-here}
